from django.conf.urls import url
from games_v3 import views


urlpatterns = [
    url(r'^games/$', views.game_list_v3),
    url(r'^games/(?P<pk>[0-9]+)/$', views.game_detail_v3),
]