from django.apps import AppConfig


class GamesV3Config(AppConfig):
    name = 'games_v3'
