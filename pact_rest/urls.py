from django.conf.urls import url, include
from django.contrib import admin


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^v1/', include('games.urls')),
    url(r'^v2/', include('games_v2.urls')),
    url(r'^v3/', include('games_v3.urls')),
    url(r'^v4/', include('games_v4.urls')),
    url(r'^v4/api-auth/', include('rest_framework.urls')),
]


