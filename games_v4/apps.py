from django.apps import AppConfig


class GamesV4Config(AppConfig):
    name = 'games_v4'
