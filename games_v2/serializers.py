from rest_framework import serializers
from games.models import Game


class GameSerializerV2(serializers.ModelSerializer):
	class Meta:
		model = Game
		fields = (
				  'id',
				  'name',
				  'release_date',
				  'game_category',
				  'played'
				  )