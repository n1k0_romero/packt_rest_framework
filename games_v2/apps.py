from django.apps import AppConfig


class GamesV2Config(AppConfig):
    name = 'games_v2'
